import React from 'react';
import styled from 'styled-components'
import { CardListas, CardViewProps } from '../core/model'
import { FaArrowLeft, FaArrowRight, FaEdit, FaTrashAlt } from "react-icons/fa";

const SCardTitle = styled.h3`
  margin: 0;
  margin-bottom: 10px;
`

const SCardBody = styled.p`
  height: 100%;
  margin: 0;
  margin-bottom: 10px;
  word-break: break-word;
  overflow-y: scroll;

`

const SCardActionsSpacer = styled.div`
  width: 45px;
  height: 30px;
  border-radius: 5px;
  opacity: 0;
`
const SCardActionsButton = styled.button`
  width: 45px;
  height: 30px;
  border-radius: 5px;
  background: #e6e6e6;
  cursor: pointer;
`

const SCardActions = styled.footer`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
`

const SCardEditButton = styled.button`
  border: none;
  background: none;
  position: absolute;
  top: 5px;
  right: 5px;
  cursor: pointer;
  font-size: 24px;
`


const SCardView = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  width: 300px;
  min-width: 300px;
  height: 200px;
  min-height: 200px;
  border-style: solid;
  border-width: 2px;
  border-radius: 3px;
  padding: 5px;
  margin-bottom: 10px;
  box-sizing: border-box;
  position: relative;
`

export function CardView({ card, moveCardBackward, moveCardForward, deleteCard, editCard }: CardViewProps) {
  return <SCardView>
    <SCardEditButton onClick={()=> editCard(card)}> 
      <FaEdit />
    </SCardEditButton>
    <SCardTitle>
      {card.titulo}
    </SCardTitle>
    <SCardBody>
      {card.conteudo}
    </SCardBody>
    <SCardActions>
      {card.lista === CardListas.TODO
        ? <SCardActionsSpacer />
        : <SCardActionsButton
          title="Move card backwards"
          onClick={(e) => moveCardBackward(card)}
        ><FaArrowLeft /></SCardActionsButton>
      }
      <SCardActionsButton title="Delete card" onClick={() => card.id ? deleteCard(card.id) : null}><FaTrashAlt /> </SCardActionsButton>
      {card.lista === CardListas.DONE
        ? <SCardActionsSpacer />
        : <SCardActionsButton
          title="Move card forwards"
          onClick={(e) => moveCardForward(card)}
        ><FaArrowRight /> </SCardActionsButton>
      }
    </SCardActions>
  </SCardView>
}