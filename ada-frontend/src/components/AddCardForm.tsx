import React, { FormEvent, useState } from 'react';
import styled from 'styled-components'
import { AddCardFormProps, Card, CardListas } from '../core/model'

const SFormItemLabel = styled.label`
    display: block;
    font-size: 16px;
    font-weight: 500;
    height: 20px;
    margin-bottom: 5px;
`
const SFormItemInput = styled.input`
    display: block;
    font-size: 18px;
    font-weight: 400;
    padding-left: 5px;
    padding-right: 10px;
    height: 40px;
    width: 100%;
    box-sizing: border-box;
`
const SFormItemArea = styled.textarea`
    display: block;
    font-size: 18px;
    font-weight: 400;
    padding-left: 5px;
    padding-right: 10px;
    height: 140px;
    width: 100%;
    box-sizing: border-box;
    resize: none;
`
const SFormItem = styled.div`
    display: flex;
    flex-flow: column nowrap;
    align-items: flex-start;
    justify-contend: flex-start;
    width: 100%;
    padding: 5px;
    box-sizing: border-box;
`

const SFormActionsButton = styled.button`
    cursor: pointer;
    width: 140px;
    height: 40px;
    ${props => props.type === 'submit' 
        ? 'border: none; background: #e5e5e5'
        : 'border-color: #e5e5e5; border-style: solid; border-width: 1px;  background: none;'
    };
`
const SFormActions = styled.footer`
    display: flex;
    flex-flow: row nowrap;
    justify-content: space-between;
    width: 100%;
    padding: 5px;
    box-sizing: border-box;
`

const SFormHeader = styled.h2`
    font-size: 24px;
    padding: 5px;
    margin-bottom: 10px;
    margin-top: 15px;
    width: 100%;
    font-weight: 500;
    box-sizing: border-box;
`

const SAddCardModalForm = styled.form`
    width: 300px;
    background: white;
  `

export function AddCardForm(props: AddCardFormProps) {

    const [card, setCardData] = useState({conteudo: '', lista: CardListas.TODO, titulo: ''} as Card)

    function onInput(event: FormEvent, field: string) {
        setCardData({...card, [field]: (event.target as HTMLInputElement).value})
    }
    async function onSubmit(event: FormEvent) {
        event.preventDefault()
        event.stopPropagation()
        await props.addCard(card)
        props.closeModal()
    }

    return <SAddCardModalForm >
        <SFormHeader>
            Add new Card
        </SFormHeader>
        <SFormItem >
            <SFormItemLabel>Title</SFormItemLabel>
            <SFormItemInput value={card.titulo} onInput={(e) => onInput(e, 'titulo')} />
        </SFormItem>
        <SFormItem >
            <SFormItemLabel>Content</SFormItemLabel>
            <SFormItemArea value={card.conteudo} onInput={(e) => onInput(e, 'conteudo')} />
        </SFormItem>
        <SFormActions>
            <SFormActionsButton type="submit" onClick={onSubmit} >Send</SFormActionsButton>
            <SFormActionsButton onClick={() => props.closeModal()}>Cancel</SFormActionsButton>
        </SFormActions>
    </SAddCardModalForm>
}