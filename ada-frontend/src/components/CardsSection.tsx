import React from 'react';
import styled from 'styled-components'
import { CardsSectionProps, Card, CardListas } from '../core/model'
import { CardView } from './CardView';

const SCardsSection = styled.section`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flext-start;
  width: 320px;
  min-width: 320px;
  max-width: 320px;
  height: 90vh;
  overflow: hidden;
  border: none;
  box-sizing: border-box;
`
const SCardsSectionCards = styled.section`
  display: flex;
  flex-flow: column nowrap;
  justify-content: flext-start;
  width: 100%;
  height: 100%;
  overflow-y: scroll;
  overflow-x: clip;
  scrollbar-width: thin;
  padding: 0 10px;
  margin: 0;
  box-sizing: border-box;
`
const SCardsSectionTitle = styled.h2`
  font-weight: 600;
  font-size: 22px;
  margin-bottom: 15px;
  margin-left: 10px;
`

export function CardsSection(props: CardsSectionProps) {

  async function moveCardForward(card: Card) {
    let forwardLista: CardListas = CardListas.DOING

    if (props.lista === CardListas.DOING) {
      forwardLista = CardListas.DONE
    }
    if (card.id) {
      await props.updateCard(card.id, { ...card, lista: forwardLista })
    }
  }

  async function moveCardBackward(card: Card) {
    let backwardLista: CardListas = CardListas.DOING

    if (props.lista === CardListas.DOING) {
      backwardLista = CardListas.TODO
    }
    if (card.id) {
      await props.updateCard(card.id, { ...card, lista: backwardLista })
    }
  }

  return (
    <SCardsSection>
      <SCardsSectionTitle>
        {props.lista.toLocaleUpperCase()}
      </SCardsSectionTitle>
      <SCardsSectionCards>
        {
          props.todos.map((card: Card) => {
            return <CardView
              key={card.id}
              card={card}
              moveCardBackward={moveCardBackward}
              moveCardForward={moveCardForward}
              deleteCard={props.deleteCard}
              editCard={props.editCard}
            />
          })
        }
      </SCardsSectionCards>

    </SCardsSection>
  );
}