import React, { useState } from 'react';
import styled from 'styled-components'
import { AddCardForm } from './AddCardForm';
import { AddCardModalProps, AddCardProps } from '../core/model'

const SAddCardModal = styled.section`
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background-color: rgba(0,0,0,0.1);
    display: flex;
    flex-flow: row nowrap;
    justify-content: center;
    align-items: center;
    z-index: 2;
  `

const SAddCardButtonIcon = styled.span`
    font-size: 22px;
    font-weight: 600;
    display: block;
    margin: 0;
    margin-bottom: 5px;
    padding: 0;

`
const SAddCardButton = styled.button`
    cursor: pointer;    
    position: absolute;
    bottom: 10px;
    right: 10px;
    width: 50px;
    height: 50px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    z-index: 1;
`


export function AddCardModal(props: AddCardModalProps) {
    return <SAddCardModal>
        <AddCardForm addCard={props.addCard} closeModal={props.closeModal} />
    </SAddCardModal>
}

export function AddCard(props: AddCardProps) {
    const [isVisible, setIsVisible] = useState(false)
    return <div>
        <SAddCardButton onClick={() => setIsVisible(!isVisible)}>
            <SAddCardButtonIcon>+</SAddCardButtonIcon>
        </SAddCardButton>
        {isVisible && <AddCardModal addCard={props.addCard} closeModal={() => setIsVisible(false)} />}
    </div>
}

