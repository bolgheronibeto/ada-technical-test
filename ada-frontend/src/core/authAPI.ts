import axios, { AxiosInstance, AxiosResponse } from "axios"

export class AuthAPI {
    private API: AxiosInstance;

    constructor() {
        this.API = axios.create({
            baseURL: 'http://localhost:5000'
        })
    }

    public async postLogin(): Promise<AxiosResponse> {
        return await this.API.post('/login/', {
        "login":"letscode", 
        "senha":"lets@123"
      })
    }
}  