import axios, { AxiosInstance, AxiosResponse } from "axios"
import { Card } from "./model";

export class CardAPI {
    private API: AxiosInstance;

    constructor(bearer: string) {
        this.API = axios.create({
            baseURL: 'http://localhost:5000',
            headers: {
                'Authorization': `Bearer ${bearer}`
            }
        })
    }

    public async deleteCard(cardId: number): Promise<AxiosResponse> {
        return await this.API.delete(`/cards/${cardId}`)
    }

    public async getCards(): Promise<AxiosResponse> {
        return await this.API.get('/cards/')

    }

    public async postCard(card: Card): Promise<AxiosResponse> {
        return await this.API.post('/cards/', { ...card });
    }

    public async putCard(cardId: number, card: Card): Promise<AxiosResponse> {
        return await this.API.put(`/cards/${cardId}`, { ...card });
    }
}  