import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';
import { CardAPI } from './core/cardAPI';

test('renders learn react link', () => {
  render(<App API={new CardAPI('nndfnausdfnasd')} />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
