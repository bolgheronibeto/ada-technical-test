import { CardAPI } from "./cardAPI"

export interface AppProps {
  API: CardAPI
}

export interface AddCardFormProps {
  addCard: (card: Card) => void,
  closeModal: () => void
}

export interface AddCardModalProps {
  addCard: (card: Card) => void,
  closeModal: () => void
}

export interface AddCardProps {
  addCard: (card: Card) => void,
}

export interface EditCardFormProps {
  card: Card,
  setCardData: (card: Card) => any,
  updateCard: (card: Card) => void,
  closeModal: () => void,
}


export interface EditCardProps {
  updateCard: (cardId: number, card: Card) => void,
  card: Card,
  isVisible: boolean,
  closeModal: () => void,
}

export interface CardViewProps {
  card: Card,
  moveCardForward: (card: Card) => any,
  moveCardBackward: (card: Card) => any,
  deleteCard: (cardId: number) => any,
  editCard: (card: Card) => any
}

export interface CardsSectionProps {
  todos: Card[],
  lista: string,
  editCard: (card: Card) => void,
  updateCard: (cardId: number, card: Card) => any,
  deleteCard: (cardId: number) => any,
}

export interface Card {
  id?: number,
  titulo: string,
  conteudo: string,
  lista: CardListas
}

export enum CardListas {
  TODO = 'todo',
  DOING = 'doing',
  DONE = 'done'
}


export const CardListasArray: Array<CardListas> = (Object.keys(CardListas) as Array<keyof typeof CardListas>).map(key => CardListas[key])

