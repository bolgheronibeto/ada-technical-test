import React, { useEffect, useState } from 'react';
import styled from 'styled-components'
import { EditCardProps } from '../core/model'
import { EditCardForm } from './EditCardForm';

const SEditCardModal = styled.section`
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw;
    height: 100vh;
    background-color: rgba(0,0,0,0.1);
    display: flex;
    flex-flow: row nowrap;
    justify-content: center;
    align-items: center;
    z-index: 2;
  `


export function EditCard({ isVisible, closeModal, updateCard: props_updateCard, card }: EditCardProps) {
    const [editingCard, setEditingCard] = useState({ ...card })

    useEffect(() => {
        setEditingCard({ ...card, })
    }, [card, isVisible])

    function updateCard() {
        if (card.id) {
            props_updateCard(card.id, editingCard)
            closeModal()
        }
    }

    return <div>
        {isVisible && (
            <SEditCardModal>
                <EditCardForm updateCard={updateCard} setCardData={setEditingCard} closeModal={closeModal} card={editingCard} />
            </SEditCardModal>
        )
        }
    </div>
}

