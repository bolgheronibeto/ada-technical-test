import React, { useEffect, useState } from 'react';
import styled from 'styled-components'
import { AddCard } from './components/AddCard';
import { CardsSection } from './components/CardsSection';
import { EditCard } from './components/EditCard';
import { AppProps, Card, CardListas, CardListasArray } from './core/model';

const SMain = styled.main`
  width: 100vw;
  max-width: 100vw;
  height: 100vh;
  max-height: 100vh;
  overflow: hidden;
  position: relative;
`
const SMainScroller = styled.section`
  width: 100%;
  min-width: 320px;
  height: 95vh;
  overflow-x: scroll;
  overflow-y: clip;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  scrollbar-width: thin;
`

function App(props: AppProps) {
  const { API } = props;
  const [todos, setTodos] = useState(new Array<Card>());
  const [editingCard, setEditingCard] = useState({ titulo: '', conteudo: '', lista: CardListas.TODO });
  const [isEditingCard, setIsEditingCard] = useState(false);

  useEffect(() => {
    async function getCards() {
      const { data } = await API.getCards()
      setTodos(data)
    }
    getCards()
  }, [API])

  async function addCard(card: Card) {
    const { data } = await API.postCard(card)
    setTodos([...todos, data])
  }
  async function updateCard(cardId: number, card: Card) {
    const { data } = await API.putCard(cardId, card)
    setTodos([...todos.filter(el => el.id !== cardId), data])
  }

  async function deleteCard(cardId: number) {
    const { data } = await API.deleteCard(cardId)
    setTodos(data)
  }

  function todoSectionFromStatus(status: CardListas) {
    return todos.filter(todo => todo.lista === status)
  }

  function startCardEdit(card: Card) {
    setEditingCard({...card});
    setIsEditingCard(true);
  }

  return (
    <SMain >
      <AddCard addCard={addCard} />
      <EditCard updateCard={updateCard} card={editingCard} isVisible={isEditingCard} closeModal={() => setIsEditingCard(false)} />
      <SMainScroller>
        {CardListasArray
          .map((lista: CardListas, key: number) => {
            return <CardsSection
              key={key + 1000}
              lista={lista}
              todos={todoSectionFromStatus(lista)}
              updateCard={updateCard}
              editCard={startCardEdit}
              deleteCard={deleteCard}
            />
          })
        }
      </SMainScroller>
    </SMain>
  );
}

export default App;
