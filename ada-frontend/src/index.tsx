import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { CardAPI } from './core/cardAPI';
import { AuthAPI } from './core/authAPI';
import { AxiosError, AxiosResponse } from 'axios';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

new AuthAPI()
  .postLogin()
  .then((response: AxiosResponse) => {
    root.render(
      <React.StrictMode>
        <App API={new CardAPI(response.data)} />
      </React.StrictMode>
    );
  })
  .catch((err : AxiosError) => {
    root.render(
      <React.StrictMode>
        <div>ERROR {err.message}</div>
      </React.StrictMode>
    );
  })

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
